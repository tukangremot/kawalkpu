import os
import time
import json
import beanstalkc
from web_kpu import WebKpu
from store import Store

beanstalk = beanstalkc.Connection(host=os.getenv('BEANSTALK_HOST', 'localhost'), port=11300)
web = WebKpu()
store = Store()

AREA_LEVEL = {
    0: ['province', 'provinces'],
    1: ['city', 'cities'],
    2: ['subdistrict', 'subdistricts'],
    3: ['village', 'villages']
}

while True:
    job = beanstalk.reserve()
    if job is None:
        web.refresh()
        time.sleep(10)
    else:
        web.refresh()
        try:
            job_data = json.loads(job.body)
            areas = job_data['value'].split("|")
            level = AREA_LEVEL[len(areas)] if len(areas) in AREA_LEVEL else []
            for area in areas:
                web.click_area(area)

            success = False
            while not success:
                rows = web.get_data()
                for row in rows:
                    if row['name'] != 'Data belum tersedia':
                        row.update({
                            '_id': "{}|{}".format(job_data['value'], row['name']),
                            '{}_id'.format(AREA_LEVEL[len(areas)-1][0]): job_data['value'],
                            'total': row['01'] + row['02']
                        })
                        store.update(level[1], row)

                        # if not level:
                        #     beanstalk.put(json.dumps({
                        #         'type': level[0],
                        #         'value': "{}|{}".format(job_data['value'], row['name'])
                        #     }))
                        success = True
                        print('success process', row['_id'])
                    else:
                        time.sleep(1)
        except Exception as e:
            print(e)
        finally:
            job.delete()
