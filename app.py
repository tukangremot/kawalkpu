from flask import Flask
from flask import render_template
from flask import request
from store import Store
from babel.numbers import format_decimal
import git
import pymongo

app = Flask(__name__)
store = Store()


def get_version():
    repo = git.Repo(search_parent_directories=True)
    version = "unreleased"
    for tag in repo.tags:
        version = tag.name
    return version


def build_data(areas):
    data = []

    total = {
        '01': 0,
        'diff_01': 0,
        'win_01': 0,

        '02': 0,
        'diff_02': 0,
        'win_02': 0,

        'draw': 0,
        'all': 0
    }
    for area in areas:
        diff_01 = 0
        color_01 = '#fff'
        color_diff_01 = '#fff'
        is_win_01 = False

        diff_02 = 0
        color_02 = '#fff'
        color_diff_02 = '#fff'
        is_win_02 = False

        if 'before' in area:
            diff_01 = area['01'] - area['before']['01']
            if diff_01 > 0:
                color_01 = '#B3EAD0'
                color_diff_01 = '#B3EAD0'
            elif diff_01 < 0:
                color_01 = '#EAB4C3'
                color_diff_01 = '#EAB4C3'

            diff_02 = area['02'] - area['before']['02']
            if diff_02 > 0:
                color_02 = '#B3EAD0'
                color_diff_02 = '#B3EAD0'
            elif diff_02 < 0:
                color_02 = '#EAB4C3'
                color_diff_02 = '#EAB4C3'

        if area['01'] > area['02']:
            is_win_01 = True
            total['win_01'] += 1
        elif area['02'] > area['01']:
            is_win_02 = True
            total['win_02'] += 1
        else:
            total['draw'] += 1

        area.update({
            'diff_01': diff_01,
            'color_01': color_01,
            'color_diff_01': color_diff_01,
            'is_win_01': is_win_01,

            'diff_02': diff_02,
            'color_02': color_02,
            'color_diff_02': color_diff_02,
            'is_win_02': is_win_02,

            'total': area['total']
        })
        data.append(area)

        total['01'] += area['01']
        total['diff_01'] += diff_01

        total['02'] += area['02']
        total['diff_02'] += diff_02
        total['all'] += area['total']

    percent = {
        '01': total['01'] * 100 / (total['01'] + total['02'])
        if total['01'] + total['02'] else 0,
        'diff_01': total['diff_01'] * 100 / (total['diff_01'] + total['diff_02'])
        if total['diff_01'] + total['diff_02'] else 0,
        'win_01': total['win_01'] * 100 / (total['draw'] + total['win_01'] + total['win_02'])
        if total['win_01'] + total['win_02'] else 0,

        '02': total['02'] * 100 / (total['01'] + total['02'])
        if total['01'] + total['02'] else 0,
        'diff_02': total['diff_02'] * 100 / (total['diff_01'] + total['diff_02'])
        if total['diff_01'] + total['diff_02'] else 0,
        'win_02': total['win_02'] * 100 / (total['draw'] + total['win_01'] + total['win_02'])
        if total['win_01'] + total['win_02'] else 0,

        'draw': total['draw'] * 100 / (total['draw'] + total['win_01'] + total['win_02'])
        if total['win_01'] + total['win_02'] else 0,
    }

    return data, total, percent


@app.route("/")
def get_provinces():
    provinces = store.db['provinces'].find()
    field = request.args.get('field')
    sort = request.args.get('sort')
    if field and sort:
        if sort == 'asc':
            sort = pymongo.ASCENDING
        else:
            sort = pymongo.DESCENDING
        provinces = provinces.sort(field, sort)
    data, total, percent = build_data(provinces)
    return render_template(
        'main.html', version=get_version(), rows=data, total=total, percent=percent, next_level='cities',
        format_decimal=format_decimal)


@app.route("/islands/<provinces>")
def get_islands_provinces(provinces):
    query = []
    provinces_list = provinces.split("|")
    for province in provinces_list:
        query.append({'_id': province})

    provinces = store.db['provinces'].find({"$or": query})
    field = request.args.get('field')
    sort = request.args.get('sort')
    if field and sort:
        if sort == 'asc':
            sort = pymongo.ASCENDING
        else:
            sort = pymongo.DESCENDING
        provinces = provinces.sort(field, sort)
    data, total, percent = build_data(provinces)
    return render_template(
        'main.html', version=get_version(), rows=data, total=total, percent=percent, next_level='cities',
        format_decimal=format_decimal)


@app.route("/cities")
def get_cities():
    cities = store.db['cities'].find()
    field = request.args.get('field')
    sort = request.args.get('sort')
    if field and sort:
        if sort == 'asc':
            sort = pymongo.ASCENDING
        else:
            sort = pymongo.DESCENDING
        cities = cities.sort(field, sort)
    data, total, percent = build_data(cities)
    return render_template(
        'main.html', version=get_version(), rows=data, total=total, percent=percent, next_level=None,
        format_decimal=format_decimal)


@app.route("/cities/<province_id>")
def get_cities_province(province_id):
    cities = store.db['cities'].find({'province_id': province_id})
    field = request.args.get('field')
    sort = request.args.get('sort')
    if field and sort:
        if sort == 'asc':
            sort = pymongo.ASCENDING
        else:
            sort = pymongo.DESCENDING
        cities = cities.sort(field, sort)
    data, total, percent = build_data(cities)
    return render_template(
        'main.html', version=get_version(), rows=data, total=total, percent=percent, next_level=None,
        format_decimal=format_decimal)


if __name__ == "__main__":
    app.run(host='0.0.0.0')
