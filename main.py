import os
import beanstalkc
import time
import json
from apscheduler.schedulers.blocking import BlockingScheduler
from web_kpu import WebKpu
from store import Store


if __name__ == '__main__':
    schedule = BlockingScheduler()
    web = WebKpu(True)
    store = Store()

    def run():
        print("Run Kawal KPU - Provinsi")
        web.refresh()
        # queue
        beanstalk = beanstalkc.Connection(host=os.getenv('BEANSTALK_HOST', 'localhost'), port=11300)

        success = False
        while not success:
            provinces = web.get_data()
            for province in provinces:
                if province['name'] != 'Data belum tersedia':
                    province.update({
                        '_id': province['name'],
                        'total': province['01'] + province['02']
                    })
                    store.update('provinces', province)
                    beanstalk.put(json.dumps({
                        'type': 'provinces',
                        'value': province['name']
                    }))
                    success = True
                    print('success process', province['name'])
                else:
                    time.sleep(1)
            web.refresh()

    # make scheduler
    schedule.add_job(run, 'cron', minute='05')
    schedule.add_job(run, 'cron', minute='20')
    schedule.add_job(run, 'cron', minute='35')
    schedule.add_job(run, 'cron', minute='50')
    schedule.start()
