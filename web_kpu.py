import os
import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

DATA_TABLE = {
    0: "name",
    1: "01",
    2: "02"
}


class WebKpu:
    before_level_data = {}

    def __init__(self, headless=False):
        if headless:
            chrome_options = Options()
            chrome_options.add_argument("--headless")
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--disable-dev-shm-usage')
            # chrome_options.binary_location = '/Applications/Google Chrome'
            self.browser = webdriver.Chrome(
                executable_path="{}/chromedriver".format(os.getcwd()), chrome_options=chrome_options)
        else:
            self.browser = webdriver.Firefox()

        self.browser.maximize_window()
        self.browser.get("https://pemilu2019.kpu.go.id/#/ppwp/hitung-suara")
        time.sleep(4)

    def refresh(self):
        self.browser.refresh()
        time.sleep(4)

    def get_data(self):
        result = []
        loading = True
        while loading:
            # print("find rows")
            result = []
            try:
                rows = self.browser.find_elements_by_tag_name('tr')
                # print("success find rows")
                for r in rows:
                    columns = r.find_elements_by_tag_name('td')
                    # print("success find columns")
                    data = {}
                    x = 0
                    for c in columns:
                        try:
                            data[DATA_TABLE[x]] = c.text if x == 0 else int(c.text.replace(".", ""))
                        except:
                            data['01'] = 0
                            data['02'] = 0
                        x += 1
                    if data:
                        result.append(data)
                loading = False
            except:
                time.sleep(1)
        return result

    def action(self, index):
        time.sleep(6)
        name = None
        rows = self.browser.find_elements_by_css_selector('.wilayah-name button')
        i = 0
        for r in rows:
            try:
                if i == index:
                    name = r.text
                    r.click()
                    break
                i += 1
            except:
                break
        return name

    def click_area(self, area):
        self.before_level_data = self.get_data()
        loading = True
        while loading:
            try:
                self.browser.execute_script("window.scrollTo(0, 710)")
                # print("find button", area)
                rows = self.browser.find_elements_by_css_selector('.wilayah-name button')
                if rows:
                    for r in rows:
                        if r.text == area:
                            time.sleep(1)
                            print("click", area)
                            r.click()
                            break
                    loading = self.before_level_data == self.get_data()
                    if not loading:
                        self.before_level_data = {}
            except:
                time.sleep(1)

    def close(self):
        self.browser.close()
