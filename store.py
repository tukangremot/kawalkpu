import os
import pymongo


class Store:
    def __init__(self):
        mongo = pymongo.MongoClient("mongodb://{}:27017/".format(os.getenv('DB_HOST', 'localhost')))
        self.db = mongo["kawalkpu"]

    def update(self, coll, data):
        data = self.check_update(coll, data)

        db_coll = self.db[coll]
        db_coll.update_one({'_id': data['_id']}, {'$set': data}, True)

    def check_update(self, coll, data):
        db_coll = self.db[coll]
        db_data = db_coll.find_one({'_id': data['_id']})
        if db_data:
            data.update({
                'before': {
                    '01': db_data['01'],
                    '02': db_data['02']
                }
            })
        return data
